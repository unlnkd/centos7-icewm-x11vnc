# centos7-icewm-x11vnc

Software include: Xvfb, x11vnc, icewm, firewalld, Google Chrome, Firefox, xterm

1. Setup server with default centos 7 image
2. Login to the server via centos user or ssh-public-key
3. Run `curl -s -L https://gitlab.com/unlnkd/centos7-icewm-x11vnc/-/raw/main/setup.sh  | bash`
4. Setup SSH-tunnel `ssh -l centos -p 22 -i .ssh/<my_public_key> -L 5900:localhost:5900 <server_ip>`
5. Connect to vnc://localhost:5900 on the local machine
