#!/bin/bash
set -e

sudo yum -y update
sudo yum -y install epel-release
sudo yum install -y icewm
sudo yum install -y x11vnc
sudo yum install -y psmisc
sudo yum install -y firewalld
sudo systemctl enable firewalld
sudo systemctl start firewalld

sudo bash -c "cat > /etc/yum.repos.d/google-chrome.repo" << EOF
[google-chrome]
name=google-chrome
baseurl=http://dl.google.com/linux/chrome/rpm/stable/x86_64
enabled=1
gpgcheck=1
gpgkey=https://dl.google.com/linux/linux_signing_key.pub
EOF

sudo yum install -y google-chrome-stable 
sudo yum install -y firefox

mkdir -p ~/.icewm

cat << EOF > ~/.icewm/menu
# This is an example for IceWM's menu definition file.
#
# Place your variants in /etc/icewm or in \$HOME/.icewm
# since modifications to this file will be discarded when you
# (re)install icewm.
#
prog xterm /usr/share/icons/gnome/16x16/apps/terminal.png xterm
prog "Chrome" /usr/share/icons/gnome/16x16/apps/web-browser.png /usr/bin/google-chrome about:blank
prog "Firefox" /usr/share/icons/gnome/16x16/apps/web-browser.png /usr/bin/firefox about:blank
separator
prog "Rebuild program menu" /usr/share/icons/gnome/16x16/actions/edit-find-replace.png /usr/share/icewm/startup
separator
menufile Programs folder programs.autogen
EOF

cat << EOF > ~/.icewm/theme
Theme="clearlooks-2px/default.theme"
EOF

cat << EOF > ~/run_vnc.sh
#!/bin/bash

if [ "\$1" == "restart" ]; then

  echo "Killall services"
  killall chrome; sleep 2
  killall firefox; sleep 2
  killall icewm; sleep 2
  killall x11vnc; sleep 2
  killall Xvfb; sleep 2

  echo "Start GUI + VNC"
  export DISPLAY=:0
  Xvfb :0 -nolisten tcp -screen 0 1280x800x24 -ac >> ~/Xvfb.log 2>&1 &
  sleep 2
  x11vnc -localhost -shared -forever -display :0 >> ~/x11vnc.log 2>&1 &
  sleep 2
  nohup icewm --display=:0 >> ~/icewm.log 2>&1 &

else
  echo "Use 'restart'"
fi
EOF

sudo bash -c "cat >> /etc/rc.d/rc.local" << EOF
su - $(whoami) -c '/home/$(whoami)/run_vnc.sh restart'
EOF

sudo chmod +x /etc/rc.d/rc.local
sudo systemctl enable rc-local

chmod +x ~/run_vnc.sh

sudo reboot
